namespace DinlenmeTesisi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tesis",
                c => new
                    {
                        tesis_id = c.Int(nullable: false, identity: true),
                        tesis_ad = c.String(),
                        tesis_adres = c.String(),
                        tesis_telefon = c.String(),
                        tesis_mail = c.String(),
                    })
                .PrimaryKey(t => t.tesis_id);
            
            CreateTable(
                "dbo.Uye",
                c => new
                    {
                        user_id = c.Int(nullable: false, identity: true),
                        username = c.String(),
                        password = c.String(),
                        phone = c.String(),
                        mail = c.String(),
                        name = c.String(),
                        lastname = c.String(),
                        role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.user_id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Uye");
            DropTable("dbo.Tesis");
        }
    }
}
