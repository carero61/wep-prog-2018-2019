﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DinlenmeTesisi.Models;
namespace DinlenmeTesisi.Controllers
{
    public class AdminController : Controller
    {
        DatabaseContext db = new DatabaseContext();
        // GET: Admin
        public ActionResult Index()
        {
            var tesisler = (from x in db.tesis select x).ToList();
            return View(tesisler);
        }
        public ActionResult sil()
        {
            var tesis_id_route = RouteData.Values["id"].ToString();
            var tesis_id = Convert.ToInt32(tesis_id_route);

            DinlenmeTesisleri tesis = (from x in db.tesis where x.tesis_id == tesis_id select x).FirstOrDefault();
            db.tesis.Remove(tesis);
            int sonuc = db.SaveChanges();
            if (sonuc > 0)
            {
                return Redirect(Request.UrlReferrer.ToString());
            }
            return View();
        }
        public ActionResult duzenle()
        {
            var tesis_id_route = RouteData.Values["id"].ToString();
            var tesis_id = Convert.ToInt32(tesis_id_route);

            DinlenmeTesisleri tesis = (from x in db.tesis where x.tesis_id == tesis_id select x).FirstOrDefault();
            ViewBag.tesisad = tesis.tesis_ad;
            ViewBag.tesisil = tesis.il;
            ViewBag.tesisilce = tesis.ilce;
            ViewBag.tesisimail = tesis.tesis_mail;
            ViewBag.tesisadres = tesis.tesis_adres;
            ViewBag.tesistelefon = tesis.tesis_telefon;

            return View();
        }
        [HttpPost]
        public ActionResult duzenle(DinlenmeTesisleri tesisler)
        {
            var tesis_id_route = RouteData.Values["id"].ToString();
            var tesis_id = Convert.ToInt32(tesis_id_route);

            DinlenmeTesisleri tesis = (from x in db.tesis where x.tesis_id == tesis_id select x).FirstOrDefault();
            ViewBag.tesisad = tesis.tesis_ad;
            ViewBag.tesisil = tesis.il;
            ViewBag.tesisilce = tesis.ilce;
            ViewBag.tesismail = tesis.tesis_mail;
            ViewBag.tesisadres = tesis.tesis_adres;
            ViewBag.tesistelefon = tesis.tesis_telefon;

            if (tesisler.tesis_ad != null)
            {
                tesis.tesis_ad = tesisler.tesis_ad;
            }
            if (tesisler.tesis_adres != null)
            {
                tesis.tesis_adres = tesisler.tesis_adres;
            }
            if (tesisler.tesis_mail != null)
            {
                tesis.tesis_mail = tesisler.tesis_mail;
            }
            if (tesisler.tesis_telefon != null)
            {
                tesis.tesis_telefon = tesisler.tesis_telefon;
            }
            if (tesisler.il != null)
            {
                tesis.il = tesisler.il;
            }
            if (tesisler.ilce != null)
            {
                tesis.ilce = tesisler.ilce;
            }

            int sonuc = db.SaveChanges();
            if (sonuc > 0)
            {
                return RedirectToAction("Index", "Admin");
            }

            return View();
        }
        public ActionResult ekle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ekle(DinlenmeTesisleri tesisler)
        {
            db.tesis.Add(tesisler);
            int sonuc = db.SaveChanges();
            if (sonuc > 0)
            {
                return RedirectToAction("Index", "Admin");
            }
            return View();
        }
        public ActionResult Uyeler()
        {
            var uyeler = db.uye.ToList();
            return View(uyeler);
        }
        public ActionResult uye_sil()
        {
            var uye_id_route = RouteData.Values["id"].ToString();
            var uye_id = Convert.ToInt32(uye_id_route);
            Uyeler uye = (from x in db.uye where x.user_id == uye_id select x).FirstOrDefault();
            db.uye.Remove(uye);
            return Redirect(Request.UrlReferrer.ToString());
        }
        public ActionResult uye_adminyap()
        {
            var uye_id_route = RouteData.Values["id"].ToString();
            var uye_id = Convert.ToInt32(uye_id_route);
            Uyeler uye = (from x in db.uye where x.user_id == uye_id select x).FirstOrDefault();
            if (uye.role == 0)
            {
                uye.role = 1;
                int sonuc = db.SaveChanges();
                if (sonuc > 0)
                {
                    return RedirectToAction("Uyeler", "Admin");
                }
            }
            else
            {
                return RedirectToAction("Uyeler", "Admin");
            }
            return View();
        }
        public ActionResult admin_uyeyap()
        {
            var uye_id_route = RouteData.Values["id"].ToString();
            var uye_id = Convert.ToInt32(uye_id_route);
            Uyeler uye = (from x in db.uye where x.user_id == uye_id select x).FirstOrDefault();
            if (uye.role == 1)
            {
                uye.role = 0;
                int sonuc = db.SaveChanges();
                if (sonuc > 0)
                {
                    return RedirectToAction("Uyeler", "Admin");
                }
            }
            else
            {
                return RedirectToAction("Uyeler", "Admin");
            }
            return View();
        }
        public ActionResult admin_yorumlar()
        {
            var yorumlar = db.yorum.ToList();
            return View(yorumlar);
        }
        public ActionResult yorum_sil()
        {
            var yorum_id = Convert.ToInt32(RouteData.Values["id"].ToString());
            Yorum yorum = db.yorum.Where(x => x.yorum_id == yorum_id).FirstOrDefault();
            db.yorum.Remove(yorum);
            int sonuc = db.SaveChanges();
            return Redirect(Request.UrlReferrer.ToString());
        }
        
    }
}