﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DinlenmeTesisi.Models;
using PagedList;
using PagedList.Mvc;

namespace DinlenmeTesisi.Controllers
{
    public class HomeController : Controller
    {
        DatabaseContext db = new DatabaseContext();
        // GET: Home
        public ActionResult Index()
        {
            var tesisler = (from x in db.tesis select x).OrderByDescending(x => x.tesis_id).Take(5);

            return View(tesisler);
        }
        public ActionResult tesis()
        {
            var tesis_id_route = RouteData.Values["id"].ToString();
            var tesis_id = Convert.ToInt32(tesis_id_route);
            Session["tesis_id"] = tesis_id;

            DinlenmeTesisleri tesis = (from x in db.tesis where x.tesis_id == tesis_id select x).FirstOrDefault();
            ViewBag.tesisad = tesis.tesis_ad;
            ViewBag.tesisil = tesis.il;
            ViewBag.tesisilce = tesis.ilce;
            ViewBag.adres = tesis.tesis_adres;

            return View();
        }
        public ActionResult tesis_yorum(int? SayfaNo)
        {
            int _sayfaNo = SayfaNo ?? 1;
            int tesis_id = Convert.ToInt32(Session["tesis_id"].ToString());
            var yorumlar = (from x in db.yorum where x.tesis.tesis_id == tesis_id select x).OrderBy(x => x.yorum_id).ToPagedList<Yorum>(_sayfaNo, 5);
            if (yorumlar != null)
            {
                return View(yorumlar);
            }
            else
            {
                return View();
            }
        }
        [HttpPost]
        public ActionResult tesis(string yorum)
        {
            var tesis_id_route = RouteData.Values["id"].ToString();
            var tesis_id = Convert.ToInt32(tesis_id_route);
            var uye_id = Convert.ToInt32(Session["user_id"]);

            Uyeler uye_x = (from x in db.uye where x.user_id == uye_id select x).FirstOrDefault();
            DinlenmeTesisleri tesis_x = (from x in db.tesis where x.tesis_id == tesis_id select x).FirstOrDefault();

            db.yorum.Add(new Yorum { uye = uye_x, yorum = yorum, tesis = tesis_x, yorum_tarihi = DateTime.Now });
            int sonuc = db.SaveChanges();
            if (sonuc > 0)
            {
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
            {
                return View();
            }

        }
        public ActionResult Anasayfa()
        {

            return View();
        }
        public ActionResult Iletisim()
        {

            return View();
        }
        public ActionResult yorumumu_sil()
        {
            var yorum_id_route = RouteData.Values["id"].ToString();
            var yorum_id = Convert.ToInt32(yorum_id_route);
            Yorum silinecek_yorum = (from x in db.yorum where x.yorum_id == yorum_id select x).FirstOrDefault();
            db.yorum.Remove(silinecek_yorum);
            db.SaveChanges();
            return Redirect(Request.UrlReferrer.ToString());
        }
        public ActionResult tesis_il()
        {
            var il = RouteData.Values["id"].ToString();
            var tesisler = (from x in db.tesis where x.il == il select x).ToList();
            ViewBag.il = il;
            return View(tesisler);
        }

    }


}