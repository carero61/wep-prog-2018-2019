﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DinlenmeTesisi.Models;

namespace DinlenmeTesisi.Controllers
{
    public class UyeController : Controller
    {
        DatabaseContext db = new DatabaseContext();
        // GET: Uye
        public ActionResult Profil()
        {
            if (Session["username"] != null)
            {
                var username = Session["username"].ToString();
                Uyeler uye = (from x in db.uye where x.username == username select x).FirstOrDefault();
                ViewBag.uye_ad = uye.name;
                ViewBag.uye_soyad = uye.lastname;
                ViewBag.uye_mail = uye.mail;
                ViewBag.uye_sifre = uye.password;
                ViewBag.uye_telefon = uye.phone;
            }
            return View();
        }
        public ActionResult Duzenle()
        {
            var username = Session["username"].ToString();
            Uyeler uye = (from x in db.uye where x.username == username select x).FirstOrDefault();
            ViewBag.uye_ad = uye.name;
            ViewBag.uye_soyad = uye.lastname;
            ViewBag.uye_mail = uye.mail;
            ViewBag.uye_sifre = uye.password;
            ViewBag.uye_telefon = uye.phone;
            return View();
        }
        [HttpPost]
        public ActionResult Duzenle(Uyeler duzenle_uye)
        {
            var username = Session["username"].ToString();
            Uyeler uye = (from x in db.uye where x.username == username select x).FirstOrDefault();
            ViewBag.uye_ad = uye.name;
            ViewBag.uye_soyad = uye.lastname;
            ViewBag.username = uye.username;
            ViewBag.uye_mail = uye.mail;
            ViewBag.uye_sifre = uye.password;
            ViewBag.uye_telefon = uye.phone;


            uye.name = ViewBag.uye_ad;
            uye.lastname = ViewBag.uye_soyad;
            uye.username = ViewBag.username;
            uye.mail = duzenle_uye.mail;
            uye.phone = duzenle_uye.phone;
            uye.password = duzenle_uye.password;
            int sonuc = db.SaveChanges();

            if (sonuc > 0)
            {
                return RedirectToAction("Profil", "Uye");
            }
            return View();
        }
        public ActionResult Uye_Ol()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Uye_Ol(Uyeler uye)
        {
            if (ModelState.IsValid)
            {
                db.uye.Add(uye);
                int sonuc = db.SaveChanges();
                if (sonuc > 0)
                {
                    return RedirectToAction("Uye_Giris", "Uye");
                }
                else
                {
                    ViewBag.uye_ekleme = "Kayıt başarısız";
                }
                return View();
            }
            else
            {
                return View();
            }
        }

        public ActionResult Uye_Giris()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Uye_Giris(Uyeler uye)
        {
            Uyeler u = (from x in db.uye where x.username == uye.username select x).FirstOrDefault();
            if (u.password == uye.password)
            {
                Session["username"] = u.username;
                Session["user_id"] = u.user_id;
                Session["role"] = u.role;
                if (u.role == 0)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return RedirectToAction("Index", "Admin");
                }
            }
            else
            {
                ViewBag.giris = "Başarısız";
            }
            return View();
        }
        public ActionResult logout()
        {
            Session["role"] = null;
            Session["username"] = null;
            return RedirectToAction("Uye_Giris", "Uye");
        }
    }
}