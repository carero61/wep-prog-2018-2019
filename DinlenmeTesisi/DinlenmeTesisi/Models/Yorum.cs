﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DinlenmeTesisi.Models
{
    [Table("Yorum")]
    public class Yorum
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int yorum_id { get; set; }
        public string yorum { get; set; }
        public DateTime yorum_tarihi { get; set; }
        public virtual Uyeler uye{ get; set; }
        public virtual DinlenmeTesisleri tesis{ get; set; }
    }
}