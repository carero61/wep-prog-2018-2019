﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using DinlenmeTesisi.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DinlenmeTesisi.Models
{
	public class DatabaseContext : DbContext
    {
        public DbSet<Uyeler> uye { get; set; }
        public DbSet<DinlenmeTesisleri> tesis { get; set; }
        public DbSet<Yorum> yorum { get; set; }
        public DatabaseContext()
        {
            Database.SetInitializer(new DatabaseCreator());
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
    public class DatabaseCreator : CreateDatabaseIfNotExists<DatabaseContext>
    {
        protected override void Seed(DatabaseContext context)
        {
            base.Seed(context);
        }
    }
}