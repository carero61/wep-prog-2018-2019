﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DinlenmeTesisi.Models
{
    [Table("Uye")]
    public class Uyeler
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int user_id { get; set; }
        
        public string username { get; set; }
        
        public string password { get; set; }
        
        public string phone { get; set; }
        
        public string mail { get; set; }
        
        public string name { get; set; }
        
        public string lastname { get; set; }

        public int role { get; set; }
    }
}