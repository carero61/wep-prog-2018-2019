﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DinlenmeTesisi.Models
{
    [Table("Tesis")]
    public class DinlenmeTesisleri
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int tesis_id { get; set; }
        public string tesis_ad { get; set; }
        public string tesis_adres { get; set; }
        public string il { get; set; }
        public string ilce { get; set; }
        public string tesis_telefon { get; set; }
        public string tesis_mail { get; set; }
    }
}